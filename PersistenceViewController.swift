//
//  PersistenceViewController.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 10/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit
var scoreItems:[String] = []
class PersistenceViewController: UIViewController, UITableViewDelegate {
    @IBAction func newGamePlayed(sender: AnyObject) {
        //reseting all the values...
        win = 0
        win2 = 0
        matchTieCount = 0
    }
    @IBOutlet var scoreTable: UITableView!
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int  // return no of table cell
    {
        return scoreItems.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->UITableViewCell   {   // setting the text label in the cell
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.sizeToFit()
        cell.textLabel?.numberOfLines = 6
        cell.textLabel?.text = scoreItems[indexPath.row]
        return cell
    }
    override func viewWillAppear(animated: Bool) {
        //cell adding function...
        if var storedtoDoItems: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("scoreTable") {
            scoreItems = []
            for index in 0..<storedtoDoItems.count {
                scoreItems.append(storedtoDoItems[index] as NSString)
            }
        }
        //reloads data in table
        scoreTable.reloadData()
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //deletes selected cell...
        if editingStyle == UITableViewCellEditingStyle.Delete {
            scoreItems.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        //updates from permanent storage...
        var fixedRecord = scoreItems
        NSUserDefaults.standardUserDefaults().setObject(fixedRecord, forKey: "scoreTable")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //return the table height
        return tableView.rowHeight
    }
    //for running into portrait only...
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
