//
//  ScoreViewController.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 05/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//
import UIKit

class ScoreViewController : UIViewController {
    
    @IBAction func allScores(sender: AnyObject) {
        self.saveRecord()  //calls the function...
    }
    func saveRecord() {
        if pressSave == 0 {
            //If user entered both names...
            if playerTwo != "" && playerOne != "" {
                let Records = "\(playerOne) won:\(playerOneScore.text!) matches \n\(playerTwo) won:\(playerTwoScore.text!) matches \nMatches draw:\(matchTieScore.text!) \nTotal games played: \(totalGamesPlayed)"
                scoreItems.append(Records)
                
                // If user entered no name...
            } else if playerTwo == "" && playerOne == "" {
                let Records = "Player One won:\(playerOneScore.text!) matches \nPlayer Two won:\(playerTwoScore.text!) matches \nMatches draw:\(matchTieScore.text!) \nTotal games played: \(totalGamesPlayed)"
                scoreItems.append(Records)
                
                //If user entered name in player one only...
            } else if playerTwo == "" && playerOne != "" {
                let Records = "\(playerOne) won:\(playerOneScore.text!) matches \nPlayer Two won:\(playerTwoScore.text!) matches \nMatches draw:\(matchTieScore.text!) \nTotal games played: \(totalGamesPlayed)"
                scoreItems.append(Records)
                //If user entered name in player two only...
            } else if playerTwo != "" && playerOne == "" {
                let Records = "Player One won:\(playerOneScore.text!) matches \n\(playerTwo) won:\(playerTwoScore.text!) matches \nMatches draw:\(matchTieScore.text!) \nTotal games played: \(totalGamesPlayed)"
                scoreItems.append(Records)
            }
            //for persistant storage...
            let storedItem = scoreItems // converting into immutable...
            NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "scoreTable")
            NSUserDefaults.standardUserDefaults().synchronize()
            pressSave++
        } else {
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "Please save the valid result", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    //labels...
    @IBOutlet weak var totalGames: UILabel!
    @IBOutlet weak var playerOneScore: UILabel!
    @IBOutlet weak var playerTwoScore: UILabel!
    @IBOutlet weak var matchTieScore: UILabel!
    
    var totalGamesPlayed = win + win2 + matchTieCount
    override func viewDidLoad() {
        super.viewDidLoad()
        //Displays the score whenever the function is loaded...
        self.playerOneScore.text = "\(win)"
        self.playerTwoScore.text = "\(win2)"
        self.matchTieScore.text = "\(matchTieCount)"
        self.totalGames.text = "\(totalGamesPlayed)"
    }
    @IBAction func quitPressed(sender: AnyObject) {
        //resets the values again to 0...
        win = 0
        win2 = 0
        matchTieCount = 0
    }
    //for running into portrait only...
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
}