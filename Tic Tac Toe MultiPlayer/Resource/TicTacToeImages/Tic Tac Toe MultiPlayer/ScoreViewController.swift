//
//  ScoreViewController.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 05/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import Foundation
import UIKit

class ScoreViewController : UIViewController {
    
    @IBOutlet weak var totalGames: UILabel!
    @IBOutlet weak var playerOneScore: UILabel!
    @IBOutlet weak var playerTwoScore: UILabel!
    @IBOutlet weak var matchTieScore: UILabel!
   
   var totalGamesPlayed = win + win2 + matchTieCount
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.playerOneScore.text = "\(win)"
        self.playerTwoScore.text = "\(win2)"
        self.matchTieScore.text = "\(matchTieCount)"
        self.totalGames.text = "\(totalGamesPlayed)"
        
    }

    @IBAction func quitPressed(sender: AnyObject) {
        win = 0
        win2 = 0
        matchTieCount = 0
    }
}