//
//  PersistantViewController.swift
//  Tic Tac Toe MultiPlayer
//
//  Created by apple on 10/02/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

class PersistantViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
